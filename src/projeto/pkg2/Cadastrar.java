/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto.pkg2;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author marcelo
 */
public class Cadastrar extends javax.swing.JInternalFrame {

    /**
     * Creates new form Cadastrar
     */
    public Cadastrar() {
        initComponents();
        FieldCarretaDisp.setText(String.valueOf(GerenciaArquivos.VeiculosDisponiveis[0]));
        FieldVanDisp.setText(String.valueOf(GerenciaArquivos.VeiculosDisponiveis[1]));
        FieldCarroDisp.setText(String.valueOf(GerenciaArquivos.VeiculosDisponiveis[2]));
        FieldMotoDisp.setText(String.valueOf(GerenciaArquivos.VeiculosDisponiveis[3]));
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        LabelCarreta = new javax.swing.JLabel();
        FieldCarreta = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        LabelVan = new javax.swing.JLabel();
        FieldVan = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        LabelCarro = new javax.swing.JLabel();
        FieldCarro = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        LabelMoto = new javax.swing.JLabel();
        FieldMoto = new javax.swing.JTextField();
        BotaoSomar4 = new javax.swing.JButton();
        BotaoSubtrair4 = new javax.swing.JButton();
        FieldMotoDisp = new javax.swing.JTextField();
        BotaoSomar3 = new javax.swing.JButton();
        BotaoSubtrair3 = new javax.swing.JButton();
        BotaoSalvar = new javax.swing.JButton();
        BotaoVoltar = new javax.swing.JButton();
        FieldCarroDisp = new javax.swing.JTextField();
        BotaoSomar2 = new javax.swing.JButton();
        BotaoSubtrair2 = new javax.swing.JButton();
        FieldVanDisp = new javax.swing.JTextField();
        BotaoSomar1 = new javax.swing.JButton();
        BotaoSubtrair1 = new javax.swing.JButton();
        FieldCarretaDisp = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();

        LabelCarreta.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelCarreta.setText("Carreta");

        FieldCarreta.setText("0");

        LabelVan.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelVan.setText("Van");

        FieldVan.setText("0");

        LabelCarro.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelCarro.setText("Carro");

        FieldCarro.setText("0");

        LabelMoto.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelMoto.setText("Moto");

        FieldMoto.setText("0");

        BotaoSomar4.setText("+");
        BotaoSomar4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotaoSomar4ActionPerformed(evt);
            }
        });

        BotaoSubtrair4.setText("-");
        BotaoSubtrair4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotaoSubtrair4ActionPerformed(evt);
            }
        });

        FieldMotoDisp.setText("0");
        FieldMotoDisp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FieldMotoDispActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(LabelMoto, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(FieldMotoDisp, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 43, Short.MAX_VALUE)
                .addComponent(BotaoSomar4, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(FieldMoto, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(BotaoSubtrair4, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(118, 118, 118))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LabelMoto, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(FieldMoto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotaoSomar4)
                    .addComponent(BotaoSubtrair4)
                    .addComponent(FieldMotoDisp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        BotaoSomar3.setText("+");
        BotaoSomar3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotaoSomar3ActionPerformed(evt);
            }
        });

        BotaoSubtrair3.setText("-");
        BotaoSubtrair3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotaoSubtrair3ActionPerformed(evt);
            }
        });

        BotaoSalvar.setText("Salvar");
        BotaoSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotaoSalvarActionPerformed(evt);
            }
        });

        BotaoVoltar.setText("Voltar");
        BotaoVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotaoVoltarActionPerformed(evt);
            }
        });

        FieldCarroDisp.setText("0");
        FieldCarroDisp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FieldCarroDispActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(BotaoVoltar, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)
                        .addComponent(BotaoSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(39, 39, 39))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(LabelCarro, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(FieldCarroDisp, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(BotaoSomar3, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(FieldCarro, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(12, 12, 12)
                        .addComponent(BotaoSubtrair3, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(115, 115, 115))))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LabelCarro, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(FieldCarro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotaoSomar3)
                    .addComponent(BotaoSubtrair3)
                    .addComponent(FieldCarroDisp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BotaoVoltar)
                    .addComponent(BotaoSalvar)))
        );

        BotaoSomar2.setText("+");
        BotaoSomar2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotaoSomar2ActionPerformed(evt);
            }
        });

        BotaoSubtrair2.setText("-");
        BotaoSubtrair2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotaoSubtrair2ActionPerformed(evt);
            }
        });

        FieldVanDisp.setText("0");
        FieldVanDisp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FieldVanDispActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(LabelVan, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(FieldVanDisp, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(46, 46, 46)
                .addComponent(BotaoSomar2, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(FieldVan, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(BotaoSubtrair2, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LabelVan, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(FieldVan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotaoSomar2)
                    .addComponent(BotaoSubtrair2)
                    .addComponent(FieldVanDisp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(129, 129, 129))
        );

        BotaoSomar1.setText("+");
        BotaoSomar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotaoSomar1ActionPerformed(evt);
            }
        });

        BotaoSubtrair1.setText("-");
        BotaoSubtrair1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotaoSubtrair1ActionPerformed(evt);
            }
        });

        FieldCarretaDisp.setText("0");
        FieldCarretaDisp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FieldCarretaDispActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(LabelCarreta, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(FieldCarretaDisp, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(47, 47, 47)
                .addComponent(BotaoSomar1, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(FieldCarreta, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(BotaoSubtrair1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LabelCarreta, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(FieldCarreta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotaoSomar1)
                    .addComponent(BotaoSubtrair1)
                    .addComponent(FieldCarretaDisp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel1.setText("Tipo");

        jLabel2.setText("Disponiveis");

        jLabel4.setText("Acrescentar");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36)
                .addComponent(jLabel2)
                .addGap(76, 76, 76)
                .addComponent(jLabel4)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 16, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 405, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BotaoVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotaoVoltarActionPerformed
        dispose();
    }//GEN-LAST:event_BotaoVoltarActionPerformed

    private void BotaoSomar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotaoSomar1ActionPerformed
        
        int valor;
        valor = Integer.parseInt(FieldCarreta.getText());
        valor++; 
        FieldCarreta.setText(String.valueOf(valor));
        
    }//GEN-LAST:event_BotaoSomar1ActionPerformed

    private void BotaoSubtrair1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotaoSubtrair1ActionPerformed

        int valor;
        valor = Integer.parseInt(FieldCarreta.getText());
        if(Integer.parseInt(FieldCarretaDisp.getText())+valor>0){
            valor--;
        }
        FieldCarreta.setText(String.valueOf(valor));
        
    }//GEN-LAST:event_BotaoSubtrair1ActionPerformed

    private void BotaoSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotaoSalvarActionPerformed
    
        int valorCarreta,valorVan,valorCarro,valorMoto;
        GerenciaArquivos arq = new GerenciaArquivos();
    
        valorCarreta = Integer.parseInt(FieldCarreta.getText());
        valorVan = Integer.parseInt(FieldVan.getText());
        valorCarro = Integer.parseInt(FieldCarro.getText());
        valorMoto = Integer.parseInt(FieldMoto.getText());
        
        GerenciaArquivos.VeiculosDisponiveis[0] += valorCarreta;
        GerenciaArquivos.VeiculosDisponiveis[1] += valorVan;
        GerenciaArquivos.VeiculosDisponiveis[2] += valorCarro;
        GerenciaArquivos.VeiculosDisponiveis[3] += valorMoto;
        
        try {
            arq.EscreveArquivoVeiculos();
        } catch (IOException ex) {
            Logger.getLogger(Cadastrar.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        dispose();
    
    }//GEN-LAST:event_BotaoSalvarActionPerformed

    private void BotaoSomar2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotaoSomar2ActionPerformed
        
        int valor;
        valor = Integer.parseInt(FieldVan.getText());
        valor++;
        FieldVan.setText(String.valueOf(valor));
        
    }//GEN-LAST:event_BotaoSomar2ActionPerformed

    private void BotaoSubtrair2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotaoSubtrair2ActionPerformed

        int valor;
        valor = Integer.parseInt(FieldVan.getText());
        if(Integer.parseInt(FieldVanDisp.getText())+valor>0){
            valor--;
        }
        FieldVan.setText(String.valueOf(valor));
        
    }//GEN-LAST:event_BotaoSubtrair2ActionPerformed

    private void BotaoSomar3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotaoSomar3ActionPerformed
        
        int valor;
        valor = Integer.parseInt(FieldCarro.getText());
        valor++;
        FieldCarro.setText(String.valueOf(valor));
        
    }//GEN-LAST:event_BotaoSomar3ActionPerformed

    private void BotaoSubtrair3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotaoSubtrair3ActionPerformed

        int valor;
        valor = Integer.parseInt(FieldCarro.getText());
        if(Integer.parseInt(FieldCarroDisp.getText())+valor>0){
            valor--;
        }
        FieldCarro.setText(String.valueOf(valor));
        
    }//GEN-LAST:event_BotaoSubtrair3ActionPerformed

    private void BotaoSomar4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotaoSomar4ActionPerformed
        
        int valor;
        valor = Integer.parseInt(FieldMoto.getText());
        valor++;
        FieldMoto.setText(String.valueOf(valor));
        
    }//GEN-LAST:event_BotaoSomar4ActionPerformed

    private void BotaoSubtrair4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotaoSubtrair4ActionPerformed

        int valor;
        valor = Integer.parseInt(FieldMoto.getText());
        if(Integer.parseInt(FieldMotoDisp.getText())+valor>0){
            valor--;
        }
        FieldMoto.setText(String.valueOf(valor));
        
    }//GEN-LAST:event_BotaoSubtrair4ActionPerformed

    private void FieldCarretaDispActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FieldCarretaDispActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_FieldCarretaDispActionPerformed

    private void FieldVanDispActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FieldVanDispActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_FieldVanDispActionPerformed

    private void FieldCarroDispActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FieldCarroDispActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_FieldCarroDispActionPerformed

    private void FieldMotoDispActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FieldMotoDispActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_FieldMotoDispActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BotaoSalvar;
    private javax.swing.JButton BotaoSomar1;
    private javax.swing.JButton BotaoSomar2;
    private javax.swing.JButton BotaoSomar3;
    private javax.swing.JButton BotaoSomar4;
    private javax.swing.JButton BotaoSubtrair1;
    private javax.swing.JButton BotaoSubtrair2;
    private javax.swing.JButton BotaoSubtrair3;
    private javax.swing.JButton BotaoSubtrair4;
    private javax.swing.JButton BotaoVoltar;
    private javax.swing.JTextField FieldCarreta;
    private javax.swing.JTextField FieldCarretaDisp;
    private javax.swing.JTextField FieldCarro;
    private javax.swing.JTextField FieldCarroDisp;
    private javax.swing.JTextField FieldMoto;
    private javax.swing.JTextField FieldMotoDisp;
    private javax.swing.JTextField FieldVan;
    private javax.swing.JTextField FieldVanDisp;
    private javax.swing.JLabel LabelCarreta;
    private javax.swing.JLabel LabelCarro;
    private javax.swing.JLabel LabelMoto;
    private javax.swing.JLabel LabelVan;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    // End of variables declaration//GEN-END:variables

}
