/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto.pkg2;

/**
 *
 * @author marcelo
 */
public class Veiculo {
    private boolean disponivel;
    private static final double ALCOOL = 3.499;
    private static final double GASOLINA = 4.449;
    private static final double DIESEL = 3.869;
    private double rendimentoPratico;

    public boolean getDisponivel() {
        return disponivel;
    }

    public void setDisponivel(boolean disponivel) {
        this.disponivel = disponivel;
    }

    public static double getALCOOL() {
        return ALCOOL;
    }

    public static double getGASOLINA() {
        return GASOLINA;
    }

    public static double getDIESEL() {
        return DIESEL;
    }

    public double getRendimentoPratico() {
        return rendimentoPratico;
    }

    public void setRendimentoPratico(double rendimentoPratico) {
        this.rendimentoPratico = rendimentoPratico;
    }
    
    

}
