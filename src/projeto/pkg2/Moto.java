/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto.pkg2;

/**
 *
 * @author marcelo
 */
public class Moto extends Veiculo{
    private static final String COMBUSTIVEL1 = "Gasolina";
    private static final String COMBUSTIVEL2 = "Alcool";
    private static final double RENDIMENTOGAS = 50;
    private static final double RENDIMENTOALC = 43;
    private static final double CARGAMAXIMA = 50;
    private static final int VELOCIDADEMEDIA = 110;
    private static final double DECAIRENDIMENTOGAS = 0.3;
    private static final double DECAIRENDIMENTOALC =  0.4;

    public static String getCOMBUSTIVEL1() {
        return COMBUSTIVEL1;
    }

    public static String getCOMBUSTIVEL2() {
        return COMBUSTIVEL2;
    }

    public static double getCARGAMAXIMA() {
        return CARGAMAXIMA;
    }

    public static int getVELOCIDADEMEDIA() {
        return VELOCIDADEMEDIA;
    }

    public static double getRENDIMENTOGAS() {
        return RENDIMENTOGAS;
    }

    public static double getRENDIMENTOALC() {
        return RENDIMENTOALC;
    }

    public static double getDECAIRENDIMENTOGAS() {
        return DECAIRENDIMENTOGAS;
    }

    public static double getDECAIRENDIMENTOALC() {
        return DECAIRENDIMENTOALC;
    }
    
    public void novoRemdimento(double carga , String combustivel){
        if(combustivel.equals(getCOMBUSTIVEL1())){
            setRendimentoPratico(getRENDIMENTOGAS()-(carga*getDECAIRENDIMENTOGAS()));
        }else{
            setRendimentoPratico(getRENDIMENTOALC()-(carga*getDECAIRENDIMENTOALC()));
        }      
    }
}
