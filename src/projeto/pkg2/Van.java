/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto.pkg2;

/**
 *
 * @author marcelo
 */
public class Van extends Veiculo{
    private static final String COMBUSTIVEL = "Diesel";
    private static final double RENDIMENTO = 10;
    private static final double CARGAMAXIMA = 3500;
    private static final int VELOCIDADEMEDIA = 80;
    private static final double DECAIRENDIMENTO = 0.001;

    public static String getCOMBUSTIVEL() {
        return COMBUSTIVEL;
    }

    public static double getCARGAMAXIMA() {
        return CARGAMAXIMA;
    }

    public static int getVELOCIDADEMEDIA() {
        return VELOCIDADEMEDIA;
    }

    public static double getRENDIMENTO() {
        return RENDIMENTO;
    }

    public static double getDECAIRENDIMENTO() {
        return DECAIRENDIMENTO;
    }
    
    public void novoRemdimento(double carga){
        setRendimentoPratico(getRENDIMENTO()-(carga*getDECAIRENDIMENTO()));
    }
    
}
