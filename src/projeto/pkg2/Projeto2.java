/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto.pkg2;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 *
 * @author marcelo
 */
public class Projeto2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {      
    GerenciaArquivos arq = new GerenciaArquivos();
    
        try {
            arq.LeArquivoVeiculos();
            arq.LeArquivoUsuario();
        } catch (IOException ex) {
            Logger.getLogger(Projeto2.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        TelaInicial telainicial = new TelaInicial();
        telainicial.setVisible(true);
    
    }
    
}
