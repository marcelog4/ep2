/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto.pkg2;

import java.io.*;
/**
 *
 * @author marcelo
 */
public class GerenciaArquivos {
    public static int[] VeiculosDisponiveis = new int[4];
    public static int[] VeiculosIndisponiveis = new int[4];
    public static double MargemLucro;
    
    
    public void LeArquivoVeiculos() throws IOException{
     
        FileInputStream entrada = new FileInputStream("src/Doc/Veiculos.txt");
        InputStreamReader entradaFormatada = new InputStreamReader(entrada);
        BufferedReader entradaString = new BufferedReader(entradaFormatada);
     
        String linha = new String();
     
        while(linha != null){
            linha = entradaString.readLine();
                if("#Veiculos disponiveis".equals(linha)){
                    linha = entradaString.readLine();
          
                    String[] linhaseparada = linha.split(" ");
                    VeiculosDisponiveis[0] = Integer.parseInt(linhaseparada[0]);
                    VeiculosDisponiveis[1] = Integer.parseInt(linhaseparada[1]);
                    VeiculosDisponiveis[2] = Integer.parseInt(linhaseparada[2]);
                    VeiculosDisponiveis[3] = Integer.parseInt(linhaseparada[3]);

                }
                if("#Veiculos nao disponiveis".equals(linha)){
                    linha = entradaString.readLine();
                    
                    String[] linhaseparada = linha.split(" ");
                    VeiculosIndisponiveis[0] = Integer.parseInt(linhaseparada[0]);
                    VeiculosIndisponiveis[1] = Integer.parseInt(linhaseparada[1]);
                    VeiculosIndisponiveis[2] = Integer.parseInt(linhaseparada[2]);
                    VeiculosIndisponiveis[3] = Integer.parseInt(linhaseparada[3]);
                }
        }
        entrada.close();
    }

    public void LeArquivoUsuario() throws IOException{
        FileInputStream entrada = new FileInputStream("src/Doc/Usuario.txt");
        InputStreamReader entradaFormatada = new InputStreamReader(entrada);
        BufferedReader entradaString = new BufferedReader(entradaFormatada);
     
        String linha = new String();
    
        while(linha != null){
            linha = entradaString.readLine();
                if("#Margem de lucro".equals(linha)){
                    linha = entradaString.readLine();
                    MargemLucro = (Double.parseDouble(linha)/100);
                }
        }
        entrada.close();
    }
    
    public void EscreveArquivoVeiculos() throws IOException{
        FileWriter saida = new FileWriter("src/Doc/Veiculos.txt");
        PrintWriter gravarSaida = new PrintWriter(saida);
        
        gravarSaida.printf("#Veiculos disponiveis%n");
        gravarSaida.printf("%d %d %d %d%n",VeiculosDisponiveis[0],VeiculosDisponiveis[1],VeiculosDisponiveis[2],VeiculosDisponiveis[3]);
        gravarSaida.printf("#Veiculos nao disponiveis%n");
        gravarSaida.printf("%d %d %d %d%n",VeiculosIndisponiveis[0],VeiculosIndisponiveis[1],VeiculosIndisponiveis[2],VeiculosIndisponiveis[3]);
        
        saida.close();
    }
    
    public void EscreveArquivoUsuario() throws IOException{
        FileWriter saida = new FileWriter("src/Doc/Usuario.txt");
        PrintWriter gravarSaida = new PrintWriter(saida);
        double valor = MargemLucro;
        valor*=100;
        
        gravarSaida.printf("#Margem de lucro%n");
        gravarSaida.printf("%s%n",String.valueOf(valor));
        
        saida.close();
    }
}
