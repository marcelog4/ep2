/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto.pkg2;

/**
 *
 * @author marcelo
 */
public class GerenciaValores {    
    private static double ValorMoto;
    private static double ValorCarro;
    private static double ValorVan;
    private static double ValorCarreta;

    public static double getValorMoto() {
        return ValorMoto;
    }

    public static void setValorMoto(double ValorMoto) {
        GerenciaValores.ValorMoto = ValorMoto;
    }

    public static double getValorCarro() {
        return ValorCarro;
    }

    public static void setValorCarro(double ValorCarro) {
        GerenciaValores.ValorCarro = ValorCarro;
    }

    public static double getValorVan() {
        return ValorVan;
    }

    public static void setValorVan(double ValorVan) {
        GerenciaValores.ValorVan = ValorVan;
    }

    public static double getValorCarreta() {
        return ValorCarreta;
    }

    public static void setValorCarreta(double ValorCarreta) {
        GerenciaValores.ValorCarreta = ValorCarreta;
    }
    
    
    public String menorCusto(double peso, double distancia , double tempo){
        double valorCarreta=999999,valorVan=999999,valorMoto=999999,valorCarro=999999,aux1=0,aux2=0;
        
        setValorMoto(valorMoto);
        setValorCarro(valorCarro);
        setValorVan(valorVan);
        setValorCarreta(valorCarreta);
        
        
        Moto moto1 = new Moto();
        Moto moto2 = new Moto();
        Carro carro1 = new Carro();
        Carro carro2 = new Carro();
        Van van = new Van();
        Carreta carreta = new Carreta();
        
        if(peso <= Moto.getCARGAMAXIMA() && GerenciaArquivos.VeiculosDisponiveis[3] > 0){
            if(verificaTempoDistancia(distancia, tempo , Moto.getVELOCIDADEMEDIA())){
                moto1.novoRemdimento(peso, "Gasolina");
                moto2.novoRemdimento(peso, "Alcool");
                
                aux1=calculaCusto(distancia, Veiculo.getGASOLINA(), moto1.getRendimentoPratico());
                aux2=calculaCusto(distancia, Veiculo.getALCOOL(), moto2.getRendimentoPratico());
                
                if(aux1>aux2){
                    valorMoto = aux2;
                    setValorMoto(valorMoto);
                }else{
                    valorMoto = aux1;
                    setValorMoto(valorMoto);
                }
            }
        }
        if(peso <= Carro.getCARGAMAXIMA() && GerenciaArquivos.VeiculosDisponiveis[2] > 0){
            if(verificaTempoDistancia(distancia, tempo , Carro.getVELOCIDADEMEDIA())){
                carro1.novoRemdimento(peso, "Gasolina");
                carro2.novoRemdimento(peso, "Alcool");
                
                aux1=calculaCusto(distancia, Veiculo.getGASOLINA(), carro1.getRendimentoPratico());
                aux2=calculaCusto(distancia, Veiculo.getALCOOL(), carro2.getRendimentoPratico());                             
                
                if(aux1>aux2){
                    valorCarro = aux2;
                    setValorCarro(valorCarro);
                }else{
                    valorCarro = aux1;
                    setValorCarro(valorCarro);
                }
            }
        }
        if(peso <= Van.getCARGAMAXIMA() && GerenciaArquivos.VeiculosDisponiveis[1] > 0){
            if(verificaTempoDistancia(distancia, tempo , Van.getVELOCIDADEMEDIA())){
                van.novoRemdimento(peso);
            
                valorVan = calculaCusto(distancia, Veiculo.getDIESEL(), van.getRendimentoPratico());
                setValorVan(valorVan);
                
            }
        }
        if(peso <= Carreta.getCARGAMAXIMA() && GerenciaArquivos.VeiculosDisponiveis[0] > 0){
            if(verificaTempoDistancia(distancia, tempo , Carreta.getVELOCIDADEMEDIA())){
                carreta.novoRemdimento(peso);
                
                valorCarreta = calculaCusto(distancia, Veiculo.getDIESEL(), carreta.getRendimentoPratico());
                setValorCarreta(valorCarreta);
                
            }
        }
        
        if(valorCarreta < valorVan && valorCarreta < valorCarro && valorCarreta < valorMoto && valorCarreta != 999999){
            return "Carreta";
        }else if(valorVan < valorCarreta && valorVan < valorCarro && valorVan < valorMoto && valorVan != 999999){
            return "Van";
        }else if(valorCarro < valorCarreta && valorCarro < valorVan && valorCarro < valorMoto && valorCarro != 999999){
            return "Carro";
        }else if(valorMoto < valorCarreta && valorMoto < valorVan && valorMoto < valorCarro && valorMoto != 999999){
            return "Moto";
        }
        
        return "Nenhum";
    }
    
    public String melhorCustoBeneficio(double peso, double distancia , double tempo){
    double valorCarreta=999999,valorVan=999999,valorMoto=999999,valorCarro=999999,aux1=0,aux2=0;
        
        Moto moto1 = new Moto();
        Moto moto2 = new Moto();
        Carro carro1 = new Carro();
        Carro carro2 = new Carro();
        Van van = new Van();
        Carreta carreta = new Carreta();
        
        if(peso <= Moto.getCARGAMAXIMA() && GerenciaArquivos.VeiculosDisponiveis[3] > 0){
            if(verificaTempoDistancia(distancia, tempo , Moto.getVELOCIDADEMEDIA())){
                moto1.novoRemdimento(peso, "Gasolina");
                moto2.novoRemdimento(peso, "Alcool");
                
                aux1=calculaCusto(distancia, Veiculo.getGASOLINA(), moto1.getRendimentoPratico());
                aux2=calculaCusto(distancia, Veiculo.getALCOOL(), moto2.getRendimentoPratico());
                
                if(aux1>aux2){
                    valorMoto = aux2;
                }else{
                    valorMoto = aux1;
                }
                valorMoto*=(distancia/Moto.getVELOCIDADEMEDIA());
            }
        }
        if(peso <= Carro.getCARGAMAXIMA() && GerenciaArquivos.VeiculosDisponiveis[2] > 0){
            if(verificaTempoDistancia(distancia, tempo , Carro.getVELOCIDADEMEDIA())){
                carro1.novoRemdimento(peso, "Gasolina");
                carro2.novoRemdimento(peso, "Alcool");
                
                aux1=calculaCusto(distancia, Veiculo.getGASOLINA(), carro1.getRendimentoPratico());
                aux2=calculaCusto(distancia, Veiculo.getALCOOL(), carro2.getRendimentoPratico());
                
                if(aux1>aux2){
                    valorCarro = aux2;
                }else{
                    valorCarro = aux1;
                }
                valorCarro*=(distancia/Carro.getVELOCIDADEMEDIA());
            }
        }
        if(peso <= Van.getCARGAMAXIMA() && GerenciaArquivos.VeiculosDisponiveis[1] > 0){
            if(verificaTempoDistancia(distancia, tempo , Van.getVELOCIDADEMEDIA())){
                van.novoRemdimento(peso);
            
                valorVan = calculaCusto(distancia, Veiculo.getDIESEL(), van.getRendimentoPratico());
                valorVan*=(distancia/Van.getVELOCIDADEMEDIA());
            }
        }
        if(peso <= Carreta.getCARGAMAXIMA() && GerenciaArquivos.VeiculosDisponiveis[0] > 0){
            if(verificaTempoDistancia(distancia, tempo , Carreta.getVELOCIDADEMEDIA())){
                carreta.novoRemdimento(peso);
                
                valorCarreta = calculaCusto(distancia, Veiculo.getDIESEL(), carreta.getRendimentoPratico());
                valorCarreta*=(distancia/Carreta.getVELOCIDADEMEDIA());
            }
        }
        
        if(valorCarreta < valorVan && valorCarreta < valorCarro && valorCarreta < valorMoto && valorCarreta != 999999){
            return "Carreta";
        }else if(valorVan < valorCarreta && valorVan < valorCarro && valorVan < valorMoto && valorVan != 999999){
            return "Van";
        }else if(valorCarro < valorCarreta && valorCarro < valorVan && valorCarro < valorMoto && valorCarro != 999999){
            return "Carro";
        }else if(valorMoto < valorCarreta && valorMoto < valorVan && valorMoto < valorCarro && valorMoto != 999999){
            return "Moto";
        }
        
        return "Nenhum";
    }
    
    public String menorTempo(double peso, double distancia , double tempo){
        if(peso <= Moto.getCARGAMAXIMA() && GerenciaArquivos.VeiculosDisponiveis[3] > 0 && peso > 0 && distancia > 0 && tempo > 0){
            if(verificaTempoDistancia(distancia, tempo , Carreta.getVELOCIDADEMEDIA())){
                return "Moto";
            }
        }
        if(peso <= Carro.getCARGAMAXIMA() && GerenciaArquivos.VeiculosDisponiveis[2] > 0 && peso > 0 && distancia > 0 && tempo > 0){
            if(verificaTempoDistancia(distancia, tempo , Carreta.getVELOCIDADEMEDIA())){
                return "Carro";
            }
        }
        if(peso <= Van.getCARGAMAXIMA() && GerenciaArquivos.VeiculosDisponiveis[1] > 0 && peso > 0 && distancia > 0 && tempo > 0){
            if(verificaTempoDistancia(distancia, tempo , Carreta.getVELOCIDADEMEDIA())){
                return "Van";
            }
        }
        if(peso <= Carreta.getCARGAMAXIMA() && GerenciaArquivos.VeiculosDisponiveis[0] > 0 && peso > 0 && distancia > 0 && tempo > 0){
            if(verificaTempoDistancia(distancia, tempo , Carreta.getVELOCIDADEMEDIA())){
                return "Carreta";
            }
        }
        return "Nenhum";
    }
    
    public boolean verificaTempoDistancia(double distancia , double tempo , double velocidade){        
         if(distancia <= tempo*velocidade){
            return true;
         }else {
             return false;
         }
    }
    
    public double calculaCusto(double distancia, double combustivel, double rend){
        return ((distancia*combustivel)/rend);
    }
    
}
